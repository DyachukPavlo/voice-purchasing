package agmedica.voicepurchasing.services;

import agmedica.voicepurchasing.domain.ShoppingCartRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ShoppingCartAPIService {

	public ShoppingCartRequest doRequest(int year)
	{
		String url = new StringBuilder()
				.append("http://shoping-cart.com/")
				.append(year)
				.append("/year?json")
				.toString();

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ShoppingCartRequest> response = restTemplate.getForEntity(url, ShoppingCartRequest.class);
				
		return response.getBody();
	}
}