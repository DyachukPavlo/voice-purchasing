package agmedica.voicepurchasing.domain;

public class ShoppingCartRequest {

	protected String text;
	protected int number;
	protected boolean found;
	protected String type;
	
	public ShoppingCartRequest() {
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}