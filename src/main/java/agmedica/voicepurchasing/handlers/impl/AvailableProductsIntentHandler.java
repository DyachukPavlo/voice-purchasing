package agmedica.voicepurchasing.handlers.impl;

import agmedica.voicepurchasing.handlers.IntentHandler;
import agmedica.voicepurchasing.services.ShoppingCartAPIService;
import agmedica.voicepurchasing.utils.AlexaUtils;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AvailableProductsIntentHandler implements IntentHandler {
	protected Logger logger = LoggerFactory.getLogger(AvailableProductsIntentHandler.class);

	private final ShoppingCartAPIService numbersService;

	public AvailableProductsIntentHandler(ShoppingCartAPIService numbersService) {
		this.numbersService = numbersService;
	}


	@Override
	public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {
		
		StringBuffer speechText = new StringBuffer();

		Slot productTypeSlot = intent.getSlot("ProductType");
		String productTypeStr = productTypeSlot == null ? null : StringUtils.trimToNull(productTypeSlot.getValue());
		
		if ( productTypeStr != null ) {
			try
			{
				//TODO request all products by type

				String answer = "Sojourn, Navis, Focusvape Pro, Zeus Armor Case";
				
				speechText.append("They have: "+ answer + ". Do you want something from this?");
			}
			catch (NumberFormatException e)
			{
				speechText.append("I do not understand what you mean by " + productTypeStr + ".  Please say a year.");
			}
		}
		else {
				speechText.append("What type of product do you interested in?");
		}
		
		Card card = AlexaUtils.newCard("ProductChoise", speechText.toString());
		PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText.toString(), AlexaUtils.inConversationMode(session));

		return AlexaUtils.newSpeechletResponse( card, speech, session, false);
	}

}