package agmedica.voicepurchasing.handlers.impl;

import agmedica.voicepurchasing.handlers.IntentHandler;
import agmedica.voicepurchasing.utils.AlexaUtils;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import org.springframework.stereotype.Component;

@Component
public class AmazonCancelIntentHandler implements IntentHandler {

	@Override
	public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {
		
		String speechText= "OK. Goodbye";
		
		Card card = AlexaUtils.newCard("See you later...", speechText);
		PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText, false);
		
		return AlexaUtils.newSpeechletResponse(card, speech, session, true);
	}

}