#!/usr/bin/env bash
#def vars
pn=$(basename $(pwd));
mvnlog='compile.log';
#bn=${pn}-build
# arch previous build
#set -e
#mvn clean verify > ${mvnlog};

#if grep -q "ERROR" "$mvnlog"; then
  #$  echo -e "\e[31m [VERIFY ERROR]\n  Look at "$mvnlog" for more \e[0m";
 #   exit 1;
#fi

if [ -f "$pn.zip" ]; then # if exist dir
mv -b "$pn.zip" "~$pn.zip";
fi

mkdir ${pn};
# build target
mvn clean package
 > ${mvnlog};

if grep -q "BUILD SUCCESS" "$mvnlog"; then
    echo -e "\e[31m  [BUILD ERROR]\n  Look at "$mvnlog" for more \e[0m";
    exit 1;
else
#    echo -e "\e[32m  [BUILD SUCCESS]\n \e[0m";
    cowsay BUILD SUCCESS
    rm -f ${mvnlog};

fi
# change the permissions
chmod +x 'target/'${pn}'.jar';
# copy
cp 'target/'${pn}'.jar' $pn/$pn'.jar';
#cp -r conf $pn/;
# archivate
#zip -r "$pn.zip" "$pn"; >> /dev/null
zip -r "$pn.zip" "$pn" >> /dev/null

if [ -d ${pn} ]; then # if exist dir
    rm -fr current-build;
    mv ${pn} current-build;
fi

#md5sum=$(md5sum $bn.zip)
#echo "$(date) $(hostname)@$(whoami) md5: $md5sum" >> "$bn.info";

#echo -e '\n\033[0;32mDONE\n'
#https://ru.wikipedia.org/wiki/Md5sum